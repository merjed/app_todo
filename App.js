import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Start from "./App/Start.js";
import List from "./App/List.js";
import Tag from "./App/Tag.js";
import NewTodo from "./App/NewTodo.js";
import EditTodo from "./App/EditTodo.js";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Constants from 'expo-constants';

const Stack = createStackNavigator();

const storeAuthDevice =  (value) => {  
     AsyncStorage.setItem('@auth_device', value)  
}

export default function App({navigation }) {
  const authID=  Constants.deviceId;
  storeAuthDevice(authID);
  return(
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false,}}>
      <Stack.Screen
          name="Accueil"
          component={Start}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen name="Tags" component={List} />
        <Stack.Screen name="New" component={NewTodo} />
        <Stack.Screen name="Edit" component={EditTodo} />
        <Stack.Screen name="Tag" component={Tag} />
        
      </Stack.Navigator>
    </NavigationContainer>
  )
}
