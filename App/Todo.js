import React, { useState }  from "react";
import { View, StyleSheet } from "react-native";
import { CheckBox } from "react-native-elements";
import Api from "../Common.js";

function Todo(props) {
  const { title, complete, id } = props;
  const completeState = complete===true  ? true : false;
  const barredText = complete===true  ? { textDecorationLine: "line-through" } : {};
  const [toggleCheckBox, setToggleCheckBox] = useState(completeState);
  const [barredStyle, setBarredStyle] = useState(barredText);
  const setBarredStyleStatus= (toggleCheckBox)=>{
      return  toggleCheckBox == false ? { textDecorationLine: "line-through" } : {}
  }

  const setMarkComplete = (id, newStatus)=>{
    fetch(`${Api}/todos/actions/${id}`, {
      method:"PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body : JSON.stringify({
        complete : newStatus
      }),
    })
    .then( res => res.json())
    .then( json =>  json);
  }

  const onChangeCheckbox=()=>
  {
    setToggleCheckBox(!toggleCheckBox);
    setBarredStyle(setBarredStyleStatus(toggleCheckBox));
    setMarkComplete(id, !toggleCheckBox);
  }
  
  return (
    <View style={styles.container}>
      <CheckBox
        styles={styles.checkbox}
        title={title}
        checked={toggleCheckBox}
        onPress={onChangeCheckbox }
        iconRight={false}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        containerStyle={styles.checkbox}
        textStyle={[styles.checkboxText, barredStyle]}
        checkedIcon="check"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  checkbox: { backgroundColor: "white", borderWidth: 0 },
  checkboxText: {
    fontWeight: "normal",
    fontSize: 16,
  },
});

export default Todo;
