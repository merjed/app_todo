import React, { useState, useEffect } from "react";
import { View, StyleSheet, TouchableHighlight } from "react-native";
import { Text } from "react-native-elements";
import TagLink from "./TagLink.js";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused } from "@react-navigation/native";
import Api from "../Common.js";


function List({ navigation }, props) {
  const [tags, setTags] = useState([]);
  const [auth, setAuth]=useState("");
  const isFocused = useIsFocused();
  const authDevice =  async () => {
    try {
      const deviceID = await AsyncStorage.getItem('@auth_device')
      setAuth(deviceID)
    } catch(e) {
        console.log('Error auth storage');
    }
  }

  const onTagsListLoad= () =>{
    fetch(`${Api}/tags/list`)
    .then((response) => response.json())
    .then((result) => {
      setTags(result);
    })
    .catch((error) => {
      console.error(error);
    });
  } 

  
  useEffect(() => {
    onTagsListLoad();
    authDevice();
  }, [isFocused])
  
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text h3 style={styles.whiteText}>
          Liste To-do
        </Text>
      </View>
      
      <View style={styles.body}>
        { tags.map(tg => {
          return(  
            <TouchableHighlight underlayColor="#fff" style={styles.touch} key={tg._id}  onPress={() => navigation.navigate('Tag', {bgColor:tg.bg_color, name:tg.title, countTasks:tg.value, tagId:tg._id})}> 
              <TagLink bgColor={tg.bg_color} name={tg.title} countTasks={tg.value} />
            </TouchableHighlight>)
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({ 
  touch:{
    flex:1
  },
  tagContainer: {
  flex: 1,
  width: "100%",
  marginBottom: 20,
  paddingLeft: 20,
  paddingRight: 20,
  flexDirection: "row",
  alignItems: "center",
},
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    backgroundColor: "#4db4f2",
    justifyContent: "center",
    alignItems: "center",
  },
  body: {
    flex: 3,
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    padding: 50,
  },
  whiteText: { color: "white" },
});

export default List;
