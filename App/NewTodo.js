import React, { useState, useEffect } from "react";
import { View, Button, StyleSheet, TextInput, Alert } from "react-native";
import { Text } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from "../Common.js";

function NewTodo(props) {
  const {navigation} = props;
  const bgColor= props.route.params!=undefined ? props.route.params.bgColor : "";
  const name= props.route.params!=undefined ? props.route.params.name : "";
  const tagId= props.route.params!=undefined ? props.route.params.tag : "";
  const [task, onChangeText] = useState("");
  const [tag, onChangeTag] = useState(tagId);
  const [tags, setTags] = useState([]);
  const [auth, setAuth]=useState("");

  const authDevice =  async () => {
    try {
      const deviceID = await AsyncStorage.getItem('@auth_device')
      setAuth(deviceID)
    } catch(e) {
        console.log('Error auth storage');
    }
  }
  const tagItems=[];
  const onTagsListLoad= () =>{
    fetch(`${Api}/tags/list`)
    .then((response) => response.json())
    .then((result) => {
      result.map((tag)=>{
        tagItems.push({label: tag.title, value:tag._id});
      })
      setTags(tagItems);
    })
    .catch((error) => {
      console.error(error);
    });
  } 

  useEffect(() => {
    // code to run on component mount
    onTagsListLoad();
    authDevice();
  }, [])


  const addTask = () => {
    if (task === "" || tag === "") {
      Alert.alert(
        "Informations requis",
        "Tag et description sont requis!",
        [{ text: "OK" }],
        { cancelable: false }
      );
    } else {
      fetch(`${Api}/todos/new/${tag}`, {
        method:"POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body : JSON.stringify({
          title : task
        }),
      })
      .then( res => res.json())
      .then( result =>{
        if(result.title!="")
        {
          if(tagId!="")
            navigation.navigate('Tag', {bgColor:bgColor, name:name, countTasks:0, tagId:tag})
          else
          navigation.navigate('Tags')
        }
      });
    }
  };

  
  return (
    <View style={styles.container}>
      <View style={[styles.header]}>
        <Text h3 style={[styles.whiteText, { marginBottom: 10 }]}>
          Nouveau To-do
        </Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.label}>Tag</Text>
        <RNPickerSelect
          onValueChange={(item) => onChangeTag(item)}
          value={tag}
          items={tags}
          style={pickerSelectStyles}
          
        />
        <Text style={styles.label}>Description</Text>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => onChangeText(text)}
          value={task}
          placeholder="Description de la tâche To-do"
          multiline={true}
          numberOfLines={5}
        />
        <Button onPress={addTask} title="+ Ajouter" color="#841584" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    backgroundColor: "#4db4f2",
    justifyContent: "center",
    alignItems: "center",
  },
  body: {
    flex: 2,
    padding: 50,
    paddingBottom: 0,
    backgroundColor: "white",
  },
  whiteText: { color: "white" },
  label: {
    fontWeight: "bold",
    fontSize: 16,
  },
  select: {
    backgroundColor: "black",
  },
  textInput: {
    height: 40,
    borderColor: "#ccc",
    borderWidth: 0,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: "black",
    fontSize: 16,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 6,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
    borderColor: "#ccc",
    borderWidth: 1,
    marginBottom: 20,
  },
});

export default NewTodo;
