import React, { useState, useEffect } from "react";
import { Image, StyleSheet, View,TouchableHighlight } from "react-native";
import { Text } from "react-native-elements";
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function Start({ navigation }) {
  const [auth, setAuth]=useState("");
  const authDevice =  async () => {
    try {
      const deviceID = await AsyncStorage.getItem('@auth_device')
      setAuth(deviceID)
    } catch(e) {
        console.log('Error auth storage');
    }
  }

  useEffect(() => {
    authDevice();
  }, [])
  
  return (
    <View style={styles.container}>
      <View style={styles.LogoContainer}>
        <Image
          source={{
            uri:
              "https://upload.wikimedia.org/wikipedia/commons/6/67/Microsoft_To-Do_icon.png",
            width: 100,
            height: 100,
          }}
        ></Image>
        <Text h4>Save what you want to do</Text>
      </View>
      
      <View style={styles.ButtonContainer}>
      <TouchableHighlight  style={[styles.button, styles.buttonBlue]} activeOpacity={0.6}
                            underlayColor="#2c5abd" onPress={() => navigation.navigate('New')}>
        <View>
          <Text style={styles.textButton}>Nouveau To-do</Text>
        </View>
      </TouchableHighlight>

      <TouchableHighlight activeOpacity={0.6}
                            underlayColor="#4db4f2" 
                            style={[styles.button, styles.buttonCyan]} onPress={() => navigation.navigate('Tags')}>
        <View >
          <Text style={styles.textButton} >Liste To-do</Text>
        </View>
      </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  LogoContainer: {
    flex: 2,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  ButtonContainer: {
    flex: 0.5,
    backgroundColor: "tomato",
    flexDirection: "row",
  },
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  textButton: {
    fontSize: 18,
    color: "white",
  },
  buttonBlue: { backgroundColor: "#2c5abd" },
  buttonCyan: { backgroundColor: "#4db4f2" },
});
