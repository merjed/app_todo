import React, {useState, useEffect} from "react";
import { View, Button, StyleSheet,Alert, FlatList } from "react-native";
import { Text } from "react-native-elements";
import Todo from "./Todo.js";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused } from "@react-navigation/native";
import Swipeout from 'react-native-swipeout';
import Api from "../Common.js";

function Tag( props) {
  const { bgColor, name, tagId } = props.route.params;
  const {navigation} = props;
  const [todos, setTodos] = useState([]);
  const [auth, setAuth]=useState("");
  const isFocused = useIsFocused();

  const authDevice =  async () => {
    try {
      const deviceID = await AsyncStorage.getItem('@auth_device')
      setAuth(deviceID)
    } catch(e) {
        console.log('Error auth storage');
    }
  }

  const onTodosListLoad= () =>{
    fetch(`${Api}/todos/list/${tagId}`)
    .then((response) => response.json())
    .then((json) => {
      setTodos(json);
    })
    .catch((error) => {
      console.error(error);
    });
  } 
  
  useEffect(() => {
    authDevice();
    onTodosListLoad();
  }, [isFocused])

  const deleteTodo = (id)=>{
    fetch(`${Api}/todos/delete/${id}`, {
      method:"DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    })
    .then( res => onTodosListLoad())
  }

  const renderRow = (rowData) => {
    const item= rowData.item;
    let swipeBtns = [
      {
        text: 'Modifier',
        backgroundColor: '#4db4f2',
        underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
        onPress: () => { navigation.navigate('Edit', {tag: tagId, bgColor:bgColor, name:name, todoId:item._id, title:item.title}) }
     },
     {
      text: 'Supprimer',
      backgroundColor: '#2c5abd',
      underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
      onPress: () => { deleteTodo(item._id); }
    }
    ];

    return (
        <Swipeout right={swipeBtns}
            autoClose={true}
            backgroundColor= 'transparent'
            key={item._id}>
            <View >
            <Todo title={item.title} complete={item.complete} id={item._id} key={item._id} />
                <View style={styles.hr} />
            </View>
        </Swipeout>
    )
  }
  
  return (
    <View style={styles.container}>
      <View style={[styles.header, { backgroundColor: bgColor }]}>
        <Text h3 style={[styles.whiteText, { marginBottom: 10 }]}>
          {name}
        </Text>
        <Button
          touchSoundDisabled={false}
          onPress={() => navigation.navigate('New', {tag: tagId, bgColor:bgColor, name:name})}
          title="+ Nouveau To-do"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
      <View style={styles.body}>
        <FlatList
        data={todos}
        renderItem={renderRow}
        keyExtractor={item=>item._id}
      />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  body: {
    flex: 2,
    padding: 5,
    paddingBottom: 0,
    backgroundColor: "white",
  },
  whiteText: { color: "white" },
  new: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 20,
  },
  new: {
    padding: 10,
    color: "black",
    fontSize: 16,
    marginTop: 20,
    backgroundColor: "white",
  },
  hr: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },
});

export default Tag;
