import React from "react";
import { View, StyleSheet, TouchableHighlight } from "react-native";
import { Text } from "react-native-elements";

function TagLink( props) {
  const { bgColor, name, countTasks } = props;
  
  return (
    <View style={[styles.tagContainer, { backgroundColor: bgColor }]}>
      <Text style={styles.textTag}>{name}</Text>
      <View style={styles.notif}>
        <Text>{countTasks}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  tag: { borderRadius: 50 },
  tagContainer: {
    flex: 1,
    width: "100%",
    marginBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  textTag: {
    fontSize: 19,
    color: "white",
    marginRight: 10,
    flex: 1,
  },
  notif: {
    backgroundColor: "white",
    height: 30,
    width: 30,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default TagLink;
